<?php

declare(strict_types=1);

namespace App\Controllers;

use Apitte\Core\Exception\Api\ClientErrorException;
use Apitte\Core\Exception\Api\ServerErrorException;
use Apitte\Core\Http\ApiRequest;
use App\Model\Entity\Task;
use App\Model\Entity\User;
use JsonSchema\Constraints\Constraint;
use JsonSchema\Validator;
use Nette\Http\IResponse;

/**
 * Class with constants and utility functions for working with tasklist API.
 */
class JsonApiUtils {
	/** API properties names **/
	public const USER_LOGIN = 'login';
	public const USER_PASSWORD = 'password';
	public const USER_CONFIRM_PASSWORD = 'confirmPassword';
	public const USER_ID = 'userId';
	public const USER_TOKEN = 'token';
	public const USER_FULL_NAME = 'fullName';
	public const TASK_USER_ID = 'assignedUserId';
	public const TASK_ID = 'taskId';
	public const TASK_RELATED_TASK_ID = 'relatedTaskId';
	public const TASK_NAME = 'name';
	public const TASK_NUMBER = 'seqNumber';
	public const TASK_ESTIMATED_TIME = 'estimatedTime';
	public const TASK_DONE = 'done';
	public const TOTAL_TIME = 'totalTime';

	/**
	 * Get and decode JSON from request body. If it does not contain a valid JSON, is is thrown error with code 400.
	 *
	 * @param ApiRequest $request
	 *
	 * @return array decoded JSON
	 */
	public function getJsonFromRequest(ApiRequest $request): array {
		$requestJson = $request->getJsonBody();
		if ($requestJson === null) {
			throw new ClientErrorException('Request body does not contain valid JSON.');
		}

		return $requestJson;
	}

	/**
	 * Extract bearer token from request header. If authorization header has invalid format, is is thrown error
	 * with code 400.
	 *
	 * @return string
	 */
	public function getBearerToken(): string {
		$matches = [];
		preg_match('/Bearer (.*)/', apache_request_headers()['Authorization'], $matches);
		if (isset($matches[1])) {
			return $matches[1];
		}

		throw new ClientErrorException('Invalid format of authorization header: Must be "Bearer [token]".',
			IResponse::S401_UNAUTHORIZED);
	}

	/**
	 * Validate JSON against the given schema and convert values to proper data types defined in the schema.
	 * Id JSON is invalid, is is thrown error with code 400.
	 *
	 * @param array $json
	 * @param object $schema
	 *
	 * @return array validated JSON with converted data types
	 */
	public function validateJson(array $json, object $schema): array {
		$validator = new Validator();
		$jsonObject = (object) $json;
		$validator->validate($jsonObject, $schema, Constraint::CHECK_MODE_COERCE_TYPES);
		if ($validator->getErrors()) {
			if (\is_array($validator->getErrors())) {
				throw new ClientErrorException('Invalid JSON - property ' . $validator->getErrors()[0]['property'] . ': ' . $validator->getErrors()[0]['message']);
			}
			throw new ClientErrorException('Invalid JSON');
		}

		$jsonString = json_encode($jsonObject);
		if (!\is_string($jsonString)) {
			throw new ServerErrorException('Error in validation JSON.');
		}

		return json_decode($jsonString, true);
	}

	/**
	 * Convert user to JSON array.
	 *
	 * @param User $user
	 *
	 * @return array
	 */
	public function convertUserToJson(User $user): array {
		$json = [];
		$json[$this::USER_ID] = $user->getId();
		$json[$this::USER_LOGIN] = $user->getLogin();
		$json[$this::USER_FULL_NAME] = $user->getFullName();

		return $json;
	}

	/**
	 * Convert task to JSON array.
	 *
	 * @param Task $task
	 *
	 * @return array
	 */
	public function convertTaskToJson(Task $task): array {
		$json = [];
		$json[$this::TASK_ID] = $task->getId();
		$json[$this::TASK_NAME] = $task->getTaskName();
		$json[$this::TASK_USER_ID] = $task->getAssignedUser()->getId();
		$json[$this::TASK_NUMBER] = $task->getSeqNumber();
		$json[$this::TASK_ESTIMATED_TIME] = $task->getEstimatedTime();
		$json[$this::TASK_DONE] = $task->isDone();
		if ($task->getPreviousRequiredTask() === null) {
			$requiredTaskId = null;
		} else {
			$requiredTaskId = $task->getPreviousRequiredTask()->getId();
		}
		$json[$this::TASK_RELATED_TASK_ID] = $requiredTaskId;

		return $json;
	}
}
