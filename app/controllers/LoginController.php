<?php

declare(strict_types=1);

namespace App\Controllers;

use Apitte\Core\Annotation\Controller\ControllerPath;
use Apitte\Core\Annotation\Controller\Method;
use Apitte\Core\Annotation\Controller\Path;
use Apitte\Core\Annotation\Controller\RequestParameter;
use Apitte\Core\Annotation\Controller\RequestParameters;
use Apitte\Core\Exception\Api\ClientErrorException;
use Apitte\Core\Exception\Api\ServerErrorException;
use Apitte\Core\Http\ApiRequest;
use Apitte\Core\Http\ApiResponse;
use App\Model\LoginManager;
use Nette\Http\IResponse;
use Nette\Security\AuthenticationException;

/**
 * @ControllerPath("/")
 */
final class LoginController extends BaseV1Controller {
	/**
	 * @var LoginManager
	 */
	private $loginManager;

	/**
	 * @var JsonApiUtils
	 */
	private $jsonApiUtils;

	/**
	 * @param LoginManager $loginManager
	 */
	public function __construct(LoginManager $loginManager) {
		$this->loginManager = $loginManager;
		$this->jsonApiUtils = new JsonApiUtils();
	}

	/**
	 * Log in user.
	 * On success return response with code 200 and JSON with logged in user (containing generated authentication token).
	 * Code 400 is returned, if request body contains invalid JSON or data is invalid. Code 401 if the credentials are
	 * invalid, code 500 on failure.
	 *
	 * @Path("/login")
	 * @Method("POST")
	 *
	 * @param ApiRequest $request
	 * @param ApiResponse $response
	 *
	 * @return ApiResponse
	 */
	public function loginUser(ApiRequest $request, ApiResponse $response): ApiResponse {
		$requestJson = $this->jsonApiUtils->getJsonFromRequest($request);
		$json = $this->jsonApiUtils->validateJson($requestJson, (object) [
				'type' => 'object',
				'required' => [$this->jsonApiUtils::USER_LOGIN, $this->jsonApiUtils::USER_PASSWORD],
				'properties' => (object) [
					$this->jsonApiUtils::USER_LOGIN => (object) [
						'type' => 'string',
						'pattern' => '.+',
					],
					$this->jsonApiUtils::USER_PASSWORD => (object) [
						'type' => 'string',
						'pattern' => '.+',
					],
				],
			]);

		try {
			$login = $json[$this->jsonApiUtils::USER_LOGIN];
			$password = $json[$this->jsonApiUtils::USER_PASSWORD];
			$identity = $this->loginManager->authenticate([$login, $password]);
		} catch (AuthenticationException $exception) {
			throw new ClientErrorException('Invalid credentials.', IResponse::S401_UNAUTHORIZED, $exception);
		} catch (\Throwable $exception) {
			throw new ServerErrorException('Error with logging in: ' . $exception->getMessage(),
					IResponse::S500_INTERNAL_SERVER_ERROR, $exception);
		}

		$responseJson = $this->jsonApiUtils->convertUserToJson($identity->getData()[$this->loginManager::IDENTITY_DATA_USER]);
		$responseJson[$this->jsonApiUtils::USER_TOKEN] = $identity->getData()[$this->loginManager::IDENTITY_DATA_TOKEN];
		$response->writeJsonBody($responseJson);

		return $response;
	}

	/**
	 * Log out user authenticated with the token.
	 * On success return response with code 204. Code 401 on invalid authentication, code 500 on failure.
	 *
	 * @Path("/logout")
	 * @Method("POST")
	 * @RequestParameters({
	 *      @RequestParameter(name="Authorization", type="string", in="header")
	 * })
	 *
	 * @param ApiRequest $request
	 * @param ApiResponse $response
	 *
	 * @return ApiResponse
	 */
	public function logoutUser(ApiRequest $request, ApiResponse $response): ApiResponse {
		$token = $this->jsonApiUtils->getBearerToken();
		$user = $this->loginManager->validateToken($token);
		if ($user === null) {
			throw new ClientErrorException('Invalid authentication token.', IResponse::S401_UNAUTHORIZED);
		}

		try {
			$this->loginManager->logout($user);
		} catch (\Throwable $exception) {
			throw new ServerErrorException('Error with logging out: ' . $exception->getMessage(),
				IResponse::S500_INTERNAL_SERVER_ERROR, $exception);
		}

		return $response->withStatus(IResponse::S204_NO_CONTENT);
	}
}
