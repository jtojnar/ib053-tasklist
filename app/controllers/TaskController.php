<?php

declare(strict_types=1);

namespace App\Controllers;

use Apitte\Core\Annotation\Controller\ControllerPath;
use Apitte\Core\Annotation\Controller\Method;
use Apitte\Core\Annotation\Controller\Path;
use Apitte\Core\Annotation\Controller\RequestParameter;
use Apitte\Core\Annotation\Controller\RequestParameters;
use Apitte\Core\Exception\Api\ClientErrorException;
use Apitte\Core\Exception\Api\ServerErrorException;
use Apitte\Core\Http\ApiRequest;
use Apitte\Core\Http\ApiResponse;
use App\Model\LoginManager;
use App\Model\TaskManager;
use App\Model\UserManager;
use Nette\Http\IResponse;

/**
 * @ControllerPath("/task")
 */
final class TaskController extends BaseV1Controller {
	/**
	 * @var UserManager
	 */
	private $userManager;

	/**
	 * @var TaskManager
	 */
	private $taskManager;

	/**
	 * @var LoginManager
	 */
	private $loginManager;

	/**
	 * @var JsonApiUtils
	 */
	private $jsonApiUtils;

	/**
	 * @param UserManager $userManager
	 * @param TaskManager $taskManager
	 * @param LoginManager $loginManager
	 */
	public function __construct(UserManager $userManager, TaskManager $taskManager, LoginManager $loginManager) {
		$this->userManager = $userManager;
		$this->taskManager = $taskManager;
		$this->loginManager = $loginManager;
		$this->jsonApiUtils = new JsonApiUtils();
	}

	/**
	 * Get task with given ID.
	 * On success return response with code 200 and JSON with the task. Code 400 is returned, if the task
	 * does not exist. Code 401 on invalid authentication, code 500 on failure.
	 *
	 * @Path("/{taskId}")
	 * @Method("GET")
	 * @RequestParameters({
	 *      @RequestParameter(name="taskId", type="int"),
	 *      @RequestParameter(name="Authorization", type="string", in="header")
	 * })
	 *
	 * @param ApiRequest $request
	 * @param ApiResponse $response
	 *
	 * @return ApiResponse
	 */
	public function getTask(ApiRequest $request, ApiResponse $response): ApiResponse {
		$token = $this->jsonApiUtils->getBearerToken();
		if ($this->loginManager->validateToken($token) === null) {
			throw new ClientErrorException('Invalid authentication token.', IResponse::S401_UNAUTHORIZED);
		}

		$id = $request->getParameter('taskId');

		try {
			$task = $this->taskManager->getById($id);
		} catch (\Throwable $exception) {
			throw new ServerErrorException('Error with getting task: ' . $exception->getMessage(),
				IResponse::S500_INTERNAL_SERVER_ERROR, $exception);
		}
		if ($task === null) {
			throw new ClientErrorException('Task with ID ' . (string) $id . ' does not exist.');
		}

		$response->writeJsonBody($this->jsonApiUtils->convertTaskToJson($task));

		return $response;
	}

	/**
	 * Update task with given ID. All the provided properties are updated.
	 * On success return response with code 200 and JSON with updated task. Code 400 is returned, if request body
	 * contains invalid JSON, data is invalid or the task does not exist. Code 401 on invalid authentication, code 500
	 * on failure.
	 *
	 * @Path("/{taskId}")
	 * @Method("PATCH")
	 * @RequestParameters({
	 *      @RequestParameter(name="taskId", type="int"),
	 *      @RequestParameter(name="Authorization", type="string", in="header")
	 * })
	 *
	 * @param ApiRequest $request
	 * @param ApiResponse $response
	 *
	 * @return ApiResponse
	 */
	public function updateTask(ApiRequest $request, ApiResponse $response): ApiResponse {
		$token = $this->jsonApiUtils->getBearerToken();
		if ($this->loginManager->validateToken($token) === null) {
			throw new ClientErrorException('Invalid authentication token.', IResponse::S401_UNAUTHORIZED);
		}

		$id = $request->getParameter('taskId');

		try {
			$task = $this->taskManager->getById($id);
		} catch (\Throwable $exception) {
			throw new ServerErrorException('Error with getting task: ' . $exception->getMessage(),
				IResponse::S500_INTERNAL_SERVER_ERROR, $exception);
		}
		if ($task === null) {
			throw new ClientErrorException('Task with ID ' . (string) $id . ' does not exist.');
		}

		$requestJson = $this->jsonApiUtils->getJsonFromRequest($request);
		$json = $this->jsonApiUtils->validateJson($requestJson, (object) [
			'type' => 'object',
			'properties' => (object) [
				$this->jsonApiUtils::TASK_NAME => (object) [
					'type' => 'string',
				],
				$this->jsonApiUtils::TASK_ESTIMATED_TIME => (object) [
					'type' => 'number',
				],
				$this->jsonApiUtils::TASK_DONE => (object) [
					'type' => 'boolean',
				],
				$this->jsonApiUtils::TASK_NUMBER => (object) [
					'type' => 'number',
				],
				$this->jsonApiUtils::TASK_RELATED_TASK_ID => (object) [
					'type' => ['number', 'null'],
				],
				$this->jsonApiUtils::TASK_USER_ID => (object) [
					'type' => 'number',
				],
			],
		]);

		if (\array_key_exists($this->jsonApiUtils::TASK_NAME, $json)) {
			$task->setTaskName($json[$this->jsonApiUtils::TASK_NAME]);
		}
		if (\array_key_exists($this->jsonApiUtils::TASK_ESTIMATED_TIME, $json)) {
			$task->setEstimatedTime($json[$this->jsonApiUtils::TASK_ESTIMATED_TIME]);
		}
		if (\array_key_exists($this->jsonApiUtils::TASK_DONE, $json)) {
			$task->setDone($json[$this->jsonApiUtils::TASK_DONE]);
		}
		if (\array_key_exists($this->jsonApiUtils::TASK_NUMBER, $json)) {
			try {
				$this->taskManager->changeSequenceNumber($task, $json[$this->jsonApiUtils::TASK_NUMBER]);
			} catch (\Throwable $exception) {
				throw new ClientErrorException('Invalid task sequence number: ' . $exception->getMessage(),
					IResponse::S400_BAD_REQUEST, $exception);
			}
		}
		if (\array_key_exists($this->jsonApiUtils::TASK_RELATED_TASK_ID, $json)) {
			if ($json[$this->jsonApiUtils::TASK_RELATED_TASK_ID] === null) {
				$relatedTask = null;
			} else {
				try {
					$relatedTask = $this->taskManager->getById($json[$this->jsonApiUtils::TASK_RELATED_TASK_ID]);
				} catch (\Throwable $exception) {
					throw new ServerErrorException('Error with getting task: ' . $exception->getMessage(),
						IResponse::S500_INTERNAL_SERVER_ERROR, $exception);
				}
				if ($relatedTask === null) {
					throw new ClientErrorException('Task with ID ' . (string) $json[$this->jsonApiUtils::TASK_RELATED_TASK_ID] . ' does not exist.');
				}
			}
			$task->setPreviousRequiredTask($relatedTask);
		}
		if (\array_key_exists($this->jsonApiUtils::TASK_USER_ID, $json)) {
			try {
				$assignedUser = $this->userManager->getById($json[$this->jsonApiUtils::TASK_USER_ID]);
			} catch (\Throwable $exception) {
				throw new ServerErrorException('Error with getting user: ' . $exception->getMessage(),
					IResponse::S500_INTERNAL_SERVER_ERROR, $exception);
			}
			if ($assignedUser === null) {
				throw new ClientErrorException('User with ID ' . (string) $json[$this->jsonApiUtils::TASK_USER_ID] . ' does not exist.');
			}
			$task->setAssignedUser($assignedUser);
		}

		try {
			$task = $this->taskManager->update($task);
		} catch (\Throwable $exception) {
			throw new ServerErrorException('Error with updating task: ' . $exception->getMessage(),
				IResponse::S500_INTERNAL_SERVER_ERROR, $exception);
		}

		$response->writeJsonBody($this->jsonApiUtils->convertTaskToJson($task));

		return $response;
	}

	/**
	 * Delete task with given ID.
	 * On success return response with code 204. Code 400 is returned, if the task does not exist. Code 401 on invalid
	 * authentication, code 500 on failure.
	 *
	 * @Path("/{taskId}")
	 * @Method("DELETE")
	 * @RequestParameters({
	 *      @RequestParameter(name="taskId", type="int"),
	 *      @RequestParameter(name="Authorization", type="string", in="header")
	 * })
	 *
	 * @param ApiRequest $request
	 * @param ApiResponse $response
	 *
	 * @return ApiResponse
	 */
	public function deleteTask(ApiRequest $request, ApiResponse $response): ApiResponse {
		$token = $this->jsonApiUtils->getBearerToken();
		if ($this->loginManager->validateToken($token) === null) {
			throw new ClientErrorException('Invalid authentication token.', IResponse::S401_UNAUTHORIZED);
		}

		$id = $request->getParameter('taskId');

		try {
			$task = $this->taskManager->getById($id);
		} catch (\Throwable $exception) {
			throw new ServerErrorException('Error with getting task: ' . $exception->getMessage(),
				IResponse::S500_INTERNAL_SERVER_ERROR, $exception);
		}
		if ($task === null) {
			throw new ClientErrorException('Task with ID ' . (string) $id . ' does not exist.');
		}

		try {
			$this->taskManager->delete($task);
		} catch (\Throwable $exception) {
			throw new ServerErrorException('Error with deleting task: ' . $exception->getMessage(),
				IResponse::S500_INTERNAL_SERVER_ERROR, $exception);
		}

		return $response->withStatus(IResponse::S204_NO_CONTENT);
	}
}
