<?php

declare(strict_types=1);

namespace App\Controllers;

use Apitte\Core\Annotation\Controller\ControllerPath;
use Apitte\Core\Annotation\Controller\Method;
use Apitte\Core\Annotation\Controller\Path;
use Apitte\Core\Annotation\Controller\RequestParameter;
use Apitte\Core\Annotation\Controller\RequestParameters;
use Apitte\Core\Exception\Api\ClientErrorException;
use Apitte\Core\Exception\Api\ServerErrorException;
use Apitte\Core\Http\ApiRequest;
use Apitte\Core\Http\ApiResponse;
use App\Exceptions\DuplicateNameException;
use App\Exceptions\ExistingRelatedTaskException;
use App\Exceptions\InvalidTaskSeqNumberException;
use App\Model\Entity\Task;
use App\Model\Entity\User;
use App\Model\LoginManager;
use App\Model\TaskManager;
use App\Model\UserManager;
use Nette\Http\IResponse;
use Nette\Security\Passwords;

/**
 * @ControllerPath("/user")
 */
final class UserController extends BaseV1Controller {
	/**
	 * @var UserManager
	 */
	private $userManager;

	/**
	 * @var TaskManager
	 */
	private $taskManager;

	/**
	 * @var LoginManager
	 */
	private $loginManager;

	/**
	 * @var JsonApiUtils
	 */
	private $jsonApiUtils;

	/**
	 * @param UserManager $userManager
	 * @param TaskManager $taskManager
	 * @param LoginManager $loginManager
	 */
	public function __construct(UserManager $userManager, TaskManager $taskManager, LoginManager $loginManager) {
		$this->userManager = $userManager;
		$this->taskManager = $taskManager;
		$this->loginManager = $loginManager;
		$this->jsonApiUtils = new JsonApiUtils();
	}

	/**
	 * Create new user with given login and password.
	 * On success return response with code 201 and JSON with created user. Code 400 is returned, if request body
	 * contains invalid JSON or data is invalid. Code 401 on invalid authentication, code 500 on failure.
	 *
	 * @Path("/")
	 * @Method("POST")
	 * @RequestParameters({
	 *      @RequestParameter(name="Authorization", type="string", in="header")
	 * })
	 *
	 * @param ApiRequest $request
	 * @param ApiResponse $response
	 *
	 * @return ApiResponse
	 */
	public function createUser(ApiRequest $request, ApiResponse $response): ApiResponse {
		$token = $this->jsonApiUtils->getBearerToken();
		if ($this->loginManager->validateToken($token) === null) {
			throw new ClientErrorException('Invalid authentication token.', IResponse::S401_UNAUTHORIZED);
		}

		$requestJson = $this->jsonApiUtils->getJsonFromRequest($request);
		$json = $this->jsonApiUtils->validateJson($requestJson, (object) [
			'type' => 'object',
			'required' => [$this->jsonApiUtils::USER_LOGIN, $this->jsonApiUtils::USER_PASSWORD, $this->jsonApiUtils::USER_CONFIRM_PASSWORD],
			'properties' => (object) [
				$this->jsonApiUtils::USER_LOGIN => (object) [
					'type' => 'string',
					'pattern' => '.+',
				],
				$this->jsonApiUtils::USER_PASSWORD => (object) [
					'type' => 'string',
					'pattern' => '.+',
				],
				$this->jsonApiUtils::USER_CONFIRM_PASSWORD => (object) [
					'type' => 'string',
					'pattern' => '.+',
				],
			],
		]);

		if ($json[$this->jsonApiUtils::USER_PASSWORD] !== $json[$this->jsonApiUtils::USER_CONFIRM_PASSWORD]) {
			throw new ClientErrorException('Passwords are not equal.');
		}
		$user = new User();
		$user
			->setLogin($json[$this->jsonApiUtils::USER_LOGIN])
			->setPassword(Passwords::hash($json[$this->jsonApiUtils::USER_PASSWORD]))
			->setFullName('');
		try {
			$user = $this->userManager->create($user);
		} catch (DuplicateNameException $exception) {
			throw new ClientErrorException($exception->getMessage(), IResponse::S400_BAD_REQUEST, $exception);
		} catch (\Throwable $exception) {
			throw new ServerErrorException('Error with creating user: ' . $exception->getMessage(),
				IResponse::S500_INTERNAL_SERVER_ERROR, $exception);
		}

		$response->withStatus(IResponse::S201_CREATED);
		$response->writeJsonBody($this->jsonApiUtils->convertUserToJson($user));

		return $response;
	}

	/**
	 * List registered users.
	 * On success return response with code 200 and JSON with the list of users.
	 * Code 401 on invalid authentication, code 500 on failure.
	 *
	 * @Path("/")
	 * @Method("GET")
	 * @RequestParameters({
	 *      @RequestParameter(name="Authorization", type="string", in="header")
	 * })
	 *
	 * @param ApiRequest $request
	 * @param ApiResponse $response
	 *
	 * @return ApiResponse
	 */
	public function listUsers(ApiRequest $request, ApiResponse $response): ApiResponse {
		$token = $this->jsonApiUtils->getBearerToken();
		if ($this->loginManager->validateToken($token) === null) {
			throw new ClientErrorException('Invalid authentication token.', IResponse::S401_UNAUTHORIZED);
		}

		try {
			$response->writeJsonBody(array_map(\Closure::fromCallable([$this->jsonApiUtils, 'convertUserToJson']), $this->userManager->getAll()));
		} catch (\Throwable $exception) {
			throw new ServerErrorException('Error getting list of users: ' . $exception->getMessage(),
				IResponse::S500_INTERNAL_SERVER_ERROR, $exception);
		}

		return $response;
	}

	/**
	 * Update user with given ID. All the provided properties are updated.
	 * On success return response with code 200 and JSON with updated user. Code 400 is returned, if request body
	 * contains invalid JSON, data is invalid or the user does not exist. Code 401 on invalid authentication, code 500
	 * on failure.
	 *
	 * @Path("/{userId}")
	 * @Method("PUT")
	 * @RequestParameters({
	 *      @RequestParameter(name="userId", type="int"),
	 *      @RequestParameter(name="Authorization", type="string", in="header")
	 * })
	 *
	 * @param ApiRequest $request
	 * @param ApiResponse $response
	 *
	 * @return ApiResponse
	 */
	public function updateUser(ApiRequest $request, ApiResponse $response): ApiResponse {
		$token = $this->jsonApiUtils->getBearerToken();
		if ($this->loginManager->validateToken($token) === null) {
			throw new ClientErrorException('Invalid authentication token.', IResponse::S401_UNAUTHORIZED);
		}

		$id = $request->getParameter('userId');
		try {
			$user = $this->userManager->getById($id);
		} catch (\Throwable $exception) {
			throw new ServerErrorException('Error with getting user: ' . $exception->getMessage(),
				IResponse::S500_INTERNAL_SERVER_ERROR, $exception);
		}
		if ($user === null) {
			throw new ClientErrorException('User with ID ' . (string) $id . ' does not exist.');
		}

		$requestJson = $this->jsonApiUtils->getJsonFromRequest($request);
		$json = $this->jsonApiUtils->validateJson($requestJson, (object) [
			'type' => 'object',
			'properties' => (object) [
				$this->jsonApiUtils::USER_LOGIN => (object) [
					'type' => 'string',
					'pattern' => '.+',
				],
				$this->jsonApiUtils::USER_PASSWORD => (object) [
					'type' => 'string',
					'pattern' => '.+',
				],
				$this->jsonApiUtils::USER_FULL_NAME => (object) [
					'type' => 'string',
					'pattern' => '.+',
				],
			],
		]);

		if (\array_key_exists($this->jsonApiUtils::USER_LOGIN, $json)) {
			$user->setLogin($json[$this->jsonApiUtils::USER_LOGIN]);
		}
		if (\array_key_exists($this->jsonApiUtils::USER_PASSWORD, $json)) {
			$user->setPassword(Passwords::hash($json[$this->jsonApiUtils::USER_PASSWORD]));
		}
		if (\array_key_exists($this->jsonApiUtils::USER_FULL_NAME, $json)) {
			$user->setFullName($json[$this->jsonApiUtils::USER_FULL_NAME]);
		}
		try {
			$user = $this->userManager->update($user);
		} catch (DuplicateNameException $exception) {
			throw new ClientErrorException($exception->getMessage(), IResponse::S400_BAD_REQUEST, $exception);
		} catch (\Throwable $exception) {
			throw new ServerErrorException('Error with updating user: ' . $exception->getMessage(),
				IResponse::S500_INTERNAL_SERVER_ERROR, $exception);
		}

		$response->writeJsonBody($this->jsonApiUtils->convertUserToJson($user));

		return $response;
	}

	/**
	 * Delete user with given ID. The user can not have any related tasks.
	 * On success return response with code 204. Code 400 is returned, if the user does not exist. Code 401 on invalid
	 * authentication, code 500 on failure.
	 *
	 * @Path("/{userId}")
	 * @Method("DELETE")
	 * @RequestParameters({
	 *      @RequestParameter(name="userId", type="int"),
	 *      @RequestParameter(name="Authorization", type="string", in="header")
	 * })
	 *
	 * @param ApiRequest $request
	 * @param ApiResponse $response
	 *
	 * @return ApiResponse
	 */
	public function deleteUser(ApiRequest $request, ApiResponse $response): ApiResponse {
		$token = $this->jsonApiUtils->getBearerToken();
		if ($this->loginManager->validateToken($token) === null) {
			throw new ClientErrorException('Invalid authentication token.', IResponse::S401_UNAUTHORIZED);
		}

		$id = $request->getParameter('userId');
		try {
			$user = $this->userManager->getById($id);
		} catch (\Throwable $exception) {
			throw new ServerErrorException('Error with getting user: ' . $exception->getMessage(),
				IResponse::S500_INTERNAL_SERVER_ERROR, $exception);
		}
		if ($user === null) {
			throw new ClientErrorException('User with ID ' . (string) $id . ' does not exist.');
		}

		try {
			$this->userManager->delete($user);
		} catch (ExistingRelatedTaskException $exception) {
			throw new ClientErrorException('Can not delete the user as he has at least one related task.',
				IResponse::S400_BAD_REQUEST, $exception);
		} catch (\Throwable $exception) {
			throw new ServerErrorException('Error with deleting user: ' . $exception->getMessage(),
				IResponse::S500_INTERNAL_SERVER_ERROR, $exception);
		}

		return $response->withStatus(IResponse::S204_NO_CONTENT);
	}

	/**
	 * Get array of all tasks for user with given ID.
	 * On success return response with code 200 and JSON with array of tasks. Code 400 is returned, if the user
	 * does not exist. Code 401 on invalid authentication, code 500 on failure.
	 *
	 * @Path("/{userId}/tasks")
	 * @Method("GET")
	 * @RequestParameters({
	 *      @RequestParameter(name="userId", type="int"),
	 *      @RequestParameter(name="Authorization", type="string", in="header")
	 * })
	 *
	 * @param ApiRequest $request
	 * @param ApiResponse $response
	 *
	 * @return ApiResponse
	 */
	public function getTasks(ApiRequest $request, ApiResponse $response): ApiResponse {
		$token = $this->jsonApiUtils->getBearerToken();
		if ($this->loginManager->validateToken($token) === null) {
			throw new ClientErrorException('Invalid authentication token.', IResponse::S401_UNAUTHORIZED);
		}

		$id = $request->getParameter('userId');

		try {
			$user = $this->userManager->getById($id);
		} catch (\Throwable $exception) {
			throw new ServerErrorException('Error with getting user: ' . $exception->getMessage(),
				IResponse::S500_INTERNAL_SERVER_ERROR, $exception);
		}
		if ($user === null) {
			throw new ClientErrorException('User with ID ' . (string) $id . ' does not exist.');
		}

		$tasks = $user->getTasks();
		$responseJson = [];
		foreach ($tasks as $task) {
			$responseJson[] = $this->jsonApiUtils->convertTaskToJson($task);
		}
		usort($responseJson, function(array $first, array $second): int {
			return $first[$this->jsonApiUtils::TASK_NUMBER] - $second[$this->jsonApiUtils::TASK_NUMBER];
		});

		$response->writeJsonBody($responseJson);

		return $response;
	}

	/**
	 * Create new task for user with given ID.
	 * On success return response with code 201 and JSON with created task. Code 400 is returned, if request body
	 * contains invalid JSON or data is invalid. Code 401 on invalid authentication, code 500 on failure.
	 *
	 * @Path("/{userId}/tasks")
	 * @Method("POST")
	 * @RequestParameters({
	 *      @RequestParameter(name="userId", type="int"),
	 *      @RequestParameter(name="Authorization", type="string", in="header")
	 * })
	 *
	 * @param ApiRequest $request
	 * @param ApiResponse $response
	 *
	 * @return ApiResponse
	 */
	public function createTask(ApiRequest $request, ApiResponse $response): ApiResponse {
		$token = $this->jsonApiUtils->getBearerToken();
		if ($this->loginManager->validateToken($token) === null) {
			throw new ClientErrorException('Invalid authentication token.', IResponse::S401_UNAUTHORIZED);
		}

		$id = $request->getParameter('userId');
		try {
			$user = $this->userManager->getById($id);
		} catch (\Throwable $exception) {
			throw new ServerErrorException('Error with getting user: ' . $exception->getMessage(),
				IResponse::S500_INTERNAL_SERVER_ERROR, $exception);
		}
		if ($user === null) {
			throw new ClientErrorException('User with ID ' . (string) $id . ' does not exist.');
		}

		$requestJson = $this->jsonApiUtils->getJsonFromRequest($request);
		$json = $this->jsonApiUtils->validateJson($requestJson, (object) [
			'type' => 'object',
			'required' => [$this->jsonApiUtils::TASK_NAME, $this->jsonApiUtils::TASK_ESTIMATED_TIME],
			'properties' => (object) [
				$this->jsonApiUtils::TASK_NAME => (object) [
					'type' => 'string',
				],
				$this->jsonApiUtils::TASK_ESTIMATED_TIME => (object) [
					'type' => 'number',
				],
				$this->jsonApiUtils::TASK_DONE => (object) [
					'type' => 'boolean',
				],
				$this->jsonApiUtils::TASK_NUMBER => (object) [
					'type' => 'number',
				],
				$this->jsonApiUtils::TASK_RELATED_TASK_ID => (object) [
					'type' => ['number', 'null'],
				],
			],
		]);

		$task = new Task();
		if (\array_key_exists($this->jsonApiUtils::TASK_DONE, $json)) {
			$done = $json[$this->jsonApiUtils::TASK_DONE];
		} else {
			$done = false;
		}
		if (\array_key_exists($this->jsonApiUtils::TASK_NUMBER, $json)) {
			$seqNumber = $json[$this->jsonApiUtils::TASK_NUMBER];
		} else {
			$seqNumber = $this->taskManager->getNextSequenceNumber($user);
		}
		if (\array_key_exists($this->jsonApiUtils::TASK_RELATED_TASK_ID, $json) &&
			$json[$this->jsonApiUtils::TASK_RELATED_TASK_ID] !== null) {
			$previousTask = $this->taskManager->getById($json[$this->jsonApiUtils::TASK_RELATED_TASK_ID]);
			if ($previousTask === null) {
				throw new ClientErrorException('Task with ID ' . (string) $json[$this->jsonApiUtils::TASK_RELATED_TASK_ID] . ' does not exist.');
			}
		} else {
			$previousTask = null;
		}

		$task
			->setTaskName($json[$this->jsonApiUtils::TASK_NAME])
			->setEstimatedTime($json[$this->jsonApiUtils::TASK_ESTIMATED_TIME])
			->setAssignedUser($user)
			->setDone($done)
			->setPreviousRequiredTask($previousTask)
			->setSeqNumber($seqNumber);

		try {
			$task = $this->taskManager->create($task);
		} catch (InvalidTaskSeqNumberException $exception) {
			throw new ClientErrorException('Invalid task sequence number: ' . $exception->getMessage(),
				IResponse::S400_BAD_REQUEST, $exception);
		} catch (\Throwable $exception) {
			throw new ServerErrorException('Error with creating task: ' . $exception->getMessage(),
				IResponse::S500_INTERNAL_SERVER_ERROR, $exception);
		}

		$response->withStatus(IResponse::S201_CREATED);
		$response->writeJsonBody($this->jsonApiUtils->convertTaskToJson($task));

		return $response;
	}

	/**
	 * Get total time of tasks assigned to the given user in seconds.
	 * On success return response with code 200 and JSON with total time. Code 400 is returned, if the user
	 * does not exist. Code 401 on invalid authentication, code 500 on failure.
	 *
	 * @Path("/{userId}/time")
	 * @Method("GET")
	 * @RequestParameters({
	 *      @RequestParameter(name="userId", type="int"),
	 *      @RequestParameter(name="Authorization", type="string", in="header")
	 * })
	 *
	 * @param ApiRequest $request
	 * @param ApiResponse $response
	 *
	 * @return ApiResponse
	 */
	public function getTotalTime(ApiRequest $request, ApiResponse $response): ApiResponse {
		$token = $this->jsonApiUtils->getBearerToken();
		if ($this->loginManager->validateToken($token) === null) {
			throw new ClientErrorException('Invalid authentication token.', IResponse::S401_UNAUTHORIZED);
		}

		$id = $request->getParameter('userId');
		try {
			$user = $this->userManager->getById($id);
		} catch (\Throwable $exception) {
			throw new ServerErrorException('Error with getting user: ' . $exception->getMessage(),
				IResponse::S500_INTERNAL_SERVER_ERROR, $exception);
		}
		if ($user === null) {
			throw new ClientErrorException('User with ID ' . (string) $id . ' does not exist.');
		}

		$time = $this->taskManager->getTotalTasksLength($user);

		$responseJson = [
			$this->jsonApiUtils::TOTAL_TIME => $time,
		];
		$response->writeJsonBody($responseJson);

		return $response;
	}
}
