<?php

declare(strict_types=1);

namespace App\Exceptions;

class ExistingRelatedTaskException extends \Exception {
}
