<?php

declare(strict_types=1);

namespace App\Model;

use App\Model\Entity\Token;
use App\Model\Entity\User;
use App\Model\Repository\TokenRepository;
use App\Model\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Nette;
use Nette\Security as NS;
use Nettrine\ORM\EntityManagerDecorator;

/**
 * Users management.
 */
final class LoginManager implements NS\IAuthenticator {
	use Nette\SmartObject;

	public const IDENTITY_DATA_USER = 'user';
	public const IDENTITY_DATA_TOKEN = 'token';

	private const COLUMN_USERNAME = 'login';

	/**
	 * @var EntityManagerDecorator
	 */
	private $entityManager;

	/**
	 * @var UserRepository
	 */
	private $userRepository;

	/**
	 * @var TokenRepository
	 */
	private $tokenRepository;

	public function __construct(EntityManagerDecorator $entityManager) {
		$this->entityManager = $entityManager;
		/** @var UserRepository $objectRepository */
		$objectRepository = $entityManager->getRepository(User::class);
		$this->userRepository = $objectRepository;
		/** @var TokenRepository $objectRepository */
		$objectRepository = $entityManager->getRepository(Token::class);
		$this->tokenRepository = $objectRepository;
	}

	/**
	 * Perform an authentication and generates an authentication token.
	 * Data array in returned Identity contains these keys: "user" => User, "token" => string.
	 *
	 * @param array $credentials
	 *
	 * @throws NS\AuthenticationException
	 * @throws \Exception
	 *
	 * @return NS\Identity
	 */
	public function authenticate(array $credentials): NS\Identity {
		[$username, $password] = $credentials;

		/** @var User|null $user */
		$user = $this->userRepository->findOneBy([self::COLUMN_USERNAME => $username]);

		if ($user === null) {
			throw new NS\AuthenticationException('The username is incorrect.', self::IDENTITY_NOT_FOUND);
		}

		if (!NS\Passwords::verify($password, $user->getPassword())) {
			throw new NS\AuthenticationException('The password is incorrect.', self::INVALID_CREDENTIAL);
		}

		if (NS\Passwords::needsRehash($user->getPassword())) {
			$user->setPassword(NS\Passwords::hash($password));
		}

		$this->logout($user);

		$tokenString = base64_encode(random_bytes(60));
		$validity = new \DateTime();
		$validity->modify('+7 days');

		$token = new Token();
		$token
			->setUser($user)
			->setTokenHash(NS\Passwords::hash($tokenString))
			->setValidUntil($validity);

		$this->entityManager->persist($token);
		$this->entityManager->flush();

		$data = [
			$this::IDENTITY_DATA_USER => $user,
			$this::IDENTITY_DATA_TOKEN => $tokenString,
		];

		return new NS\Identity($user->getId(), 'dummyUserRole', $data);
	}

	/**
	 * Check if is the token valid and return the user who the token belongs to. If the token is invalid, return NULL.
	 *
	 * @param string $token
	 *
	 * @return User|null
	 */
	public function validateToken(string $token): ?User {
		$tokenObjects = new ArrayCollection($this->tokenRepository->findAll());
		/** @var Token $tokenObject */
		foreach ($tokenObjects as $tokenObject) {
			if (NS\Passwords::verify($token, $tokenObject->getTokenHash())
				&& $tokenObject->getValidUntil()->diff(new \DateTime('now'))->invert === 1) {   // Check if date validUntil is after date now (it means that the interval returned by diff method is inverted).
				return $tokenObject->getUser();
			}
		}

		return null;
	}

	/**
	 * Logout user (there will be no valid token for the user).
	 *
	 * @param User $user
	 */
	public function logout(User $user): void {
		$tokens = $user->getTokens();

		foreach ($tokens as $token) {
			$this->entityManager->remove($token);
		}

		$this->entityManager->flush();
	}
}
