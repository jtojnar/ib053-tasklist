<?php

declare(strict_types=1);

namespace App\Model;

use App\Exceptions\InvalidTaskSeqNumberException;
use App\Model\Entity\Task;
use App\Model\Entity\User;
use App\Model\Repository\TaskRepository;
use Doctrine\Common\Collections\Collection;
use Nettrine\ORM\EntityManagerDecorator;

/**
 * Task management.
 */
class TaskManager {
	/**
	 * @var EntityManagerDecorator
	 */
	private $entityManager;

	/**
	 * @var TaskRepository
	 */
	private $taskRepository;

	public function __construct(EntityManagerDecorator $entityManager) {
		$this->entityManager = $entityManager;
		/** @var TaskRepository $objectRepository */
		$objectRepository = $entityManager->getRepository(Task::class);
		$this->taskRepository = $objectRepository;
	}

	/**
	 * @param int $id
	 *
	 * @return Task|null
	 */
	public function getById(int $id): ?Task {
		/** @var Task|null $task */
		$task = $this->taskRepository->find($id);

		return $task;
	}

	/**
	 * Create new task. Including correct sequence numbers shift.
	 *
	 * @param Task $task
	 *
	 * @throws InvalidTaskSeqNumberException
	 *
	 * @return Task
	 */
	public function create(Task $task): Task {
		$tasks = $task->getAssignedUser()->getTasks();
		$this->validateSequenceNumber($tasks, $task->getSeqNumber(), true);
		$this->shiftSequenceNumbers(
			$tasks,
			$this->getNextSequenceNumber($task->getAssignedUser()),
			$task->getSeqNumber()
		);

		$this->entityManager->persist($task);
		$this->entityManager->flush();

		return $task;
	}

	/**
	 * Update task.
	 * (Not including sequence number shifting or validation. For this use changeSequenceNumber function.).
	 *
	 * @param Task $task
	 *
	 * @return Task
	 */
	public function update(Task $task): Task {
		/** @var Task $mergedTask */
		$mergedTask = $this->entityManager->merge($task);
		$this->entityManager->flush();

		return $mergedTask;
	}

	/**
	 * Delete task. Including correct sequence numbers shift.
	 *
	 * @param Task $task
	 */
	public function delete(Task $task): void {
		$this->shiftSequenceNumbers(
			$task->getAssignedUser()->getTasks(),
			$task->getSeqNumber(),
			$this->getNextSequenceNumber($task->getAssignedUser()) - 1
		);

		$this->entityManager->remove($task);
		$this->entityManager->flush();
	}

	/**
	 * Get total estimated time of all tasks related with given user.
	 *
	 * @param User $user
	 *
	 * @return int
	 */
	public function getTotalTasksLength(User $user): int {
		$tasks = $user->getTasks();
		$time = 0;
		foreach ($tasks as $task) {
			$time += $task->getEstimatedTime();
		}

		return $time;
	}

	/**
	 * Set sequence number of task to given number. Number of all tasks assigned to the same user with equal or greater
	 * number than is new number of the given task is increased by one.
	 * This is just auxiliary function for setting properties, changes are not reflected in the database!
	 *
	 * @param Task $task
	 * @param int $seqNumber
	 *
	 * @throws InvalidTaskSeqNumberException
	 */
	public function changeSequenceNumber(Task $task, int $seqNumber): void {
		$tasks = $task->getAssignedUser()->getTasks();
		$this->validateSequenceNumber($tasks, $seqNumber);
		$this->shiftSequenceNumbers($tasks, $task->getSeqNumber(), $seqNumber);
		$task->setSeqNumber($seqNumber);
	}

	/**
	 * Return number greater by one than is the biggest sequence number of task assigned to the given user.
	 * Assumes the numbers form a sequence starting from 1.
	 *
	 * @param User $user
	 *
	 * @return int
	 */
	public function getNextSequenceNumber(User $user): int {
		$tasks = $user->getTasks();

		return $tasks->count() + 1;
	}

	/**
	 * Validate sequence number for the given tasks list. If it is for the new added task, the number can be greater
	 * by one than is the count of tasks, otherwise it must be in the range from 1 to count of tasks.
	 * If the number is invalid, exception is thrown.
	 *
	 * @param Collection $tasks
	 * @param int $seqNumber
	 * @param bool $isAddingNew
	 *
	 * @throws InvalidTaskSeqNumberException
	 */
	private function validateSequenceNumber(Collection $tasks, int $seqNumber, bool $isAddingNew = false): void {
		$maxNumber = $tasks->count() + ($isAddingNew ? 1 : 0);
		if ($seqNumber < 1) {
			throw new InvalidTaskSeqNumberException('Task\'s sequence number must be at least one.');
		}
		if ($seqNumber > $maxNumber) {
			throw new InvalidTaskSeqNumberException('Task\'s sequence number ' . (string) $seqNumber . ' is too big.');
		}
	}

	/**
	 * Use if is changed a sequence number of a task. This method changes numbers of the other tasks, so that it will
	 * (after the update) be again a sequence (if before was). The update from $prevSeqNumber to $newSeqNumber is not
	 * part of this function! (If the given tasks collection includes also the updated task, its number will be invalid
	 * and must be set to the right value after execution of this function.)
	 * This is just auxiliary function for setting properties, changes are not reflected in the database!
	 *
	 * @param Collection|Task[] $tasks
	 * @param int $prevSeqNumber
	 * @param int $newSeqNumber
	 */
	private function shiftSequenceNumbers(Collection $tasks, int $prevSeqNumber, int $newSeqNumber): void {
		if ($prevSeqNumber < $newSeqNumber) {
			$lowerBound = $prevSeqNumber;
			$upperBound = $newSeqNumber;
			$shift = -1;
		} elseif ($prevSeqNumber > $newSeqNumber) {
			$lowerBound = $newSeqNumber;
			$upperBound = $prevSeqNumber;
			$shift = 1;
		} else {
			return;
		}

		foreach ($tasks as $task) {
			$seqNumber = $task->getSeqNumber();
			if ($seqNumber >= $lowerBound && $seqNumber <= $upperBound) {
				$task->setSeqNumber($seqNumber + $shift);
			}
		}
	}
}
