<?php

declare(strict_types=1);

namespace App\Model;

use App\Exceptions\DuplicateNameException;
use App\Exceptions\ExistingRelatedTaskException;
use App\Model\Entity\User;
use App\Model\Repository\UserRepository;
use Doctrine;
use Nettrine\ORM\EntityManagerDecorator;

/**
 * Users management.
 */
class UserManager {
	/**
	 * @var EntityManagerDecorator
	 */
	private $entityManager;

	/**
	 * @var UserRepository
	 */
	private $userRepository;

	public function __construct(EntityManagerDecorator $entityManager) {
		$this->entityManager = $entityManager;
		/** @var UserRepository $objectRepository */
		$objectRepository = $entityManager->getRepository(User::class);
		$this->userRepository = $objectRepository;
	}

	/**
	 * Find user with given ID. If user with the ID does not exist, NULL is returned.
	 *
	 * @param int $id
	 *
	 * @return User|null
	 */
	public function getById(int $id): ?User {
		/** @var User|null $user */
		$user = $this->userRepository->find($id);

		return $user;
	}

	/**
	 * Find all registered users.
	 *
	 * @return User[]
	 */
	public function getAll(): array {
		/** @var User[] $user */
		$users = $this->userRepository->findAll();

		return $users;
	}

	/**
	 * Create new user. User login must be unique.
	 *
	 * @param User $user
	 *
	 * @throws DuplicateNameException
	 *
	 * @return User
	 */
	public function create(User $user): User {
		try {
			$this->entityManager->persist($user);
			$this->entityManager->flush();
		} catch (Doctrine\DBAL\Exception\UniqueConstraintViolationException $exception) {
			throw new DuplicateNameException('User with login ' . $user->getLogin() . ' already exists.', 0, $exception);
		}

		return $user;
	}

	/**
	 * Update user. User login must be unique.
	 *
	 * @param User $user
	 *
	 * @throws DuplicateNameException
	 *
	 * @return User
	 */
	public function update(User $user): User {
		try {
			/** @var User $mergedUser */
			$mergedUser = $this->entityManager->merge($user);
			$this->entityManager->flush();
		} catch (Doctrine\DBAL\Exception\UniqueConstraintViolationException $exception) {
			throw new DuplicateNameException('User with login ' . $user->getLogin() . ' already exists.', 0, $exception);
		}

		return $mergedUser;
	}

	/**
	 * Delete user. The user can not have any related tasks.
	 *
	 * @param User $user
	 *
	 * @throws ExistingRelatedTaskException
	 */
	public function delete(User $user): void {
		if (!$user->getTasks()->isEmpty()) {
			throw new ExistingRelatedTaskException('Cannot delete user that has related tasks.');
		}
		$this->entityManager->remove($user);
		$this->entityManager->flush();
	}
}
