<?php

declare(strict_types=1);

namespace App\Model\Entity;

use Doctrine\ORM\Mapping as ORM;
use Nettrine\ORM\Entity\Attributes\Id;

/**
 * @ORM\Entity(repositoryClass="App\Model\Repository\TaskRepository")
 * @ORM\Table(name="task")
 */
class Task {
	use Id;

	/**
	 * @var string
	 * @ORM\Column(name="task_name", type="string", nullable=false)
	 */
	private $taskName;

	/**
	 * @var User
	 * @ORM\ManyToOne(targetEntity="User", inversedBy="tasks")
	 * @ORM\JoinColumn(name="assigned_user_id", referencedColumnName="id", nullable=false)
	 */
	private $assignedUser;

	/**
	 * @var Task|null
	 * @ORM\ManyToOne(targetEntity="Task")
	 * @ORM\JoinColumn(name="previous_required_task_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
	 */
	private $previousRequiredTask;

	/**
	 * @var int
	 * @ORM\Column(name="seq_number", type="integer", nullable=false)
	 */
	private $seqNumber;

	/**
	 * @var int
	 * @ORM\Column(name="estimated_time", type="integer", nullable=false)
	 */
	private $estimatedTime;

	/**
	 * @var bool
	 * @ORM\Column(name="done", type="boolean", nullable=false)
	 */
	private $done;

	/**
	 * @return int
	 */
	public function getId(): int {
		return $this->id;
	}

	/**
	 * @param int $id
	 *
	 * @return Task
	 */
	public function setId(int $id): self {
		$this->id = $id;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getTaskName(): string {
		return $this->taskName;
	}

	/**
	 * @param string $taskName
	 *
	 * @return Task
	 */
	public function setTaskName(string $taskName): self {
		$this->taskName = $taskName;

		return $this;
	}

	/**
	 * @return User
	 */
	public function getAssignedUser(): User {
		return $this->assignedUser;
	}

	/**
	 * @param User $assignedUser
	 *
	 * @return Task
	 */
	public function setAssignedUser(User $assignedUser): self {
		$this->assignedUser = $assignedUser;

		return $this;
	}

	/**
	 * @return Task|null
	 */
	public function getPreviousRequiredTask(): ?self {
		return $this->previousRequiredTask;
	}

	/**
	 * @param Task|null $previousRequiredTask
	 *
	 * @return Task
	 */
	public function setPreviousRequiredTask(?self $previousRequiredTask): self {
		$this->previousRequiredTask = $previousRequiredTask;

		return $this;
	}

	/**
	 * @return int
	 */
	public function getSeqNumber(): int {
		return $this->seqNumber;
	}

	/**
	 * @param int $seqNumber
	 *
	 * @return Task
	 */
	public function setSeqNumber(int $seqNumber): self {
		$this->seqNumber = $seqNumber;

		return $this;
	}

	/**
	 * @return int
	 */
	public function getEstimatedTime(): int {
		return $this->estimatedTime;
	}

	/**
	 * @param int $estimatedTime
	 *
	 * @return Task
	 */
	public function setEstimatedTime(int $estimatedTime): self {
		$this->estimatedTime = $estimatedTime;

		return $this;
	}

	/**
	 * @return bool
	 */
	public function isDone(): bool {
		return $this->done;
	}

	/**
	 * @param bool $done
	 *
	 * @return Task
	 */
	public function setDone(bool $done): self {
		$this->done = $done;

		return $this;
	}
}
