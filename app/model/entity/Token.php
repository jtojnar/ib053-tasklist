<?php

declare(strict_types=1);

namespace App\Model\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Model\Repository\TokenRepository")
 * @ORM\Table(name="token")
 */
class Token {
	/**
	 * @var string
	 * @ORM\Column(name="token_hash", type="string", nullable=false, unique=true)
	 * @ORM\Id
	 */
	private $tokenHash;

	/**
	 * @var \DateTime
	 * @ORM\Column(name="valid_until", type="datetime", nullable=false)
	 */
	private $validUntil;

	/**
	 * @var User
	 * @ORM\ManyToOne(targetEntity="User", inversedBy="tokens")
	 * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
	 */
	private $user;

	/**
	 * @return string
	 */
	public function getTokenHash(): string {
		return $this->tokenHash;
	}

	/**
	 * @param string $tokenHash
	 *
	 * @return Token
	 */
	public function setTokenHash(string $tokenHash): self {
		$this->tokenHash = $tokenHash;

		return $this;
	}

	/**
	 * @return \DateTime
	 */
	public function getValidUntil(): \DateTime {
		return $this->validUntil;
	}

	/**
	 * @param \DateTime $validUntil
	 *
	 * @return Token
	 */
	public function setValidUntil(\DateTime $validUntil): self {
		$this->validUntil = $validUntil;

		return $this;
	}

	/**
	 * @return User
	 */
	public function getUser(): User {
		return $this->user;
	}

	/**
	 * @param User $user
	 *
	 * @return Token
	 */
	public function setUser(User $user): self {
		$this->user = $user;

		return $this;
	}
}
