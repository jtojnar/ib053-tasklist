<?php

declare(strict_types=1);

namespace App\Model\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Nettrine\ORM\Entity\Attributes\Id;

/**
 * @ORM\Entity(repositoryClass="App\Model\Repository\UserRepository")
 * @ORM\Table(name="user")
 */
class User {
	use Id;

	/**
	 * @var string
	 * @ORM\Column(name="login", type="string", nullable=false)
	 */
	private $login;

	/**
	 * @var string
	 * @ORM\Column(name="password", type="string", nullable=false)
	 */
	private $password;

	/**
	 * @var string
	 * @ORM\Column(name="full_name", type="string", nullable=false)
	 */
	private $fullName;

	/**
	 * @var Collection|Task[]
	 * @ORM\OneToMany(targetEntity="Task", mappedBy="assignedUser")
	 */
	private $tasks;

	/**
	 * @var Collection|Token[]
	 * @ORM\OneToMany(targetEntity="Token", mappedBy="user", cascade={"remove"})
	 */
	private $tokens;

	/**
	 * @return int
	 */
	public function getId(): int {
		return $this->id;
	}

	/**
	 * @param int $id
	 *
	 * @return User
	 */
	public function setId(int $id): self {
		$this->id = $id;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getLogin(): string {
		return $this->login;
	}

	/**
	 * @param string $login
	 *
	 * @return User
	 */
	public function setLogin(string $login): self {
		$this->login = $login;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getPassword(): string {
		return $this->password;
	}

	/**
	 * @param string $password
	 *
	 * @return User
	 */
	public function setPassword(string $password): self {
		$this->password = $password;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getFullName(): string {
		return $this->fullName;
	}

	/**
	 * @param string $fullName
	 *
	 * @return User
	 */
	public function setFullName(string $fullName): self {
		$this->fullName = $fullName;

		return $this;
	}

	/**
	 * @return Task[]|Collection
	 */
	public function getTasks(): Collection {
		return $this->tasks;
	}

	/**
	 * @return Token[]|Collection
	 */
	public function getTokens(): Collection {
		return $this->tokens;
	}
}
