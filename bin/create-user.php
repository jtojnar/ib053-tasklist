<?php

declare(strict_types=1);

use App\Model\Entity\User;
use Nette\Security\Passwords;

require __DIR__ . '/../vendor/autoload.php';

$container = App\Booting::boot()
	->createContainer();

if (!isset($_SERVER['argv'][3])) {
	echo '
Add new user to database.

Usage: create-user.php <name> <login> <password>
';
	exit(1);
}

[, $name, $login, $password] = $_SERVER['argv'];

$manager = $container->getByType(App\Model\UserManager::class);

try {
	$user = new User();
	$user->setFullName($name);
	$user->setLogin($login);
	$user->setPassword(Passwords::hash($password));
	$manager->create($user);
	echo "User $login was added.\n";
} catch (App\Exceptions\DuplicateNameException $e) {
	echo "Error: duplicate login.\n";
	exit(1);
}
