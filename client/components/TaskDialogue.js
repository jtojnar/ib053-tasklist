import PropTypes from 'prop-types';
import React from 'react';
import { Form, Button, Modal } from 'react-bootstrap';

class TaskDialogue extends React.Component {
	static propTypes = {
		apiBase: PropTypes.string.isRequired,
		apiToken: PropTypes.string.isRequired,
		onHide: PropTypes.func,
		userId: PropTypes.number,
		task: PropTypes.shape({
			taskId: PropTypes.number,
			relatedTaskId: PropTypes.number,
		}),
		reloadTasks: PropTypes.func,
	};

	constructor(props) {
		super(props);
		this.state = {
			error: null,
			submitError: null,
			isLoaded: false,
			users: [],
			tasks: [],

			handledTask: props.task ? {
				...props.task,
				relatedTaskId: props.task.relatedTaskId === null ? -1 : props.task.relatedTaskId,
			} : {
				name: '',
				assignedUserId: null,
				seqNumber: 0,
				estimatedTime: 0,
				relatedTaskId: -1,
				done: false
			},
		};

		this.handleTaskChange = this.handleTaskChange.bind(this);
		this.handleNumTaskChange = this.handleNumTaskChange.bind(this);
		this.handleStringTaskChange = this.handleStringTaskChange.bind(this);
		this.handleTimeChange = this.handleTimeChange.bind(this);
		this.submitTask = this.submitTask.bind(this);
	}

	componentDidMount() {
		const { apiBase, apiToken, userId } = this.props;
		Promise.all([
			fetch(`${apiBase}/user`, {
				headers: {
					Authorization: `Bearer ${apiToken}`,
				},
			}).then((users) => users.json()),
			fetch(`${apiBase}/user/${userId}/tasks`, {
				headers: {
					Authorization: `Bearer ${apiToken}`,
				},
			}).then((tasks) => tasks.json()),
		]).then(
			([users, tasks]) => {
				this.setState({
					isLoaded: true,
					users,
					tasks: [
						{taskId: -1, name: '---'},
						...tasks
					],
					handledTask: {
						...this.state.handledTask,
						assignedUserId: userId
					}
				});
			},
			// Note: it's important to handle errors here
			// instead of a catch() block so that we don't swallow
			// exceptions from actual bugs in components.
			(error) => {
				this.setState({
					isLoaded: true,
					error,
				});
			}
		);
	}

	handleTaskChange(key, value) {
		this.setState( prevState => ({ handledTask :
			{
				...prevState.handledTask,
				[key]: value
			}
		}));
	}

	handleStringTaskChange(e) {
		const key = e.target.id;
		const value = e.target.value;
		this.handleTaskChange(key, value);
	}

	handleNumTaskChange(e) {
		const key = e.target.id;
		const value = parseInt(e.target.value);
		console.log(value);
		this.handleTaskChange(key, value);
	}

	handleTimeChange(e) {
		const key = e.target.id;
		const value = e.target.value;
		const chunks = value.split(':');
		const seconds = parseInt(chunks[0]) * (60 * 60) + parseInt(chunks[1]) * 60;
		this.handleTaskChange(key, seconds);
	}

	static secToTime(duration) {
		let minutes = Math.floor((duration / 60) % 60),
			hours = Math.floor((duration / (60 * 60)) % 24);

		hours = (hours < 10) ? '0' + hours : hours;
		minutes = (minutes < 10) ? '0' + minutes : minutes;

		return hours + ':' + minutes;
	}


	submitTask(event) {
		event.preventDefault();
		const { apiBase, apiToken, onHide, task, userId, reloadTasks } = this.props;
		const { handledTask } = this.state;

		let body = {
			...handledTask,
			relatedTaskId: handledTask.relatedTaskId === -1 ? null : handledTask.relatedTaskId,
		};

		const handledTaskInfo = task ? {
			method: 'PATCH',
			address: `task/${task.taskId}`
		} : {
			method: 'POST',
			address: `user/${userId}/tasks`
		};

		Promise.all([
			fetch(new Request(
				`${apiBase}/` + handledTaskInfo.address,
				{
					method: handledTaskInfo.method,
					body: JSON.stringify(body),
					headers: {
						Authorization: `Bearer ${apiToken}`,
					},
				})
			).then((resp) => resp.json()),
		]).then(
			([response]) => {
				if (response.status === 'error') {
					this.setState({
						submitError: {
							message: response.message,
							code: response.code,
						},
					});
				} else {
					reloadTasks();
					onHide();
				}
			},
			// Note: it's important to handle errors here
			// instead of a catch() block so that we don't swallow
			// exceptions from actual bugs in components.
			(submitError) => {
				this.setState({
					submitError,
				});
			}
		);
	}

	render() {
		const { error, isLoaded, users, tasks, handledTask, submitError } = this.state;
		let body;
		if (error) {
			body = <div>Error: {error.message}</div>;
		} else if (!isLoaded) {
			// TODO: connect with the spinner
			body = <div>Loading…</div>;
		} else {
			body = (
				<Form onSubmit={this.submitTask}>
					{submitError && <div className="alert alert-danger" role="alert">
						{submitError.message}
					</div>}
					<Form.Group controlId="name">
						<Form.Label>Title</Form.Label>
						<Form.Control
							type="text"
							value={handledTask.name}
							onChange={this.handleStringTaskChange}
						/>
					</Form.Group>
					<Form.Group controlId="assignedUserId">
						<Form.Label>Assigned user</Form.Label>
						<Form.Control
							as="select"
							value={handledTask.assignedUserId}
							onChange={this.handleNumTaskChange}
						>
							{
								users.map((user, i) =>
									<option key={i} value={user.userId}>{user.fullName}</option>)
							}
						</Form.Control>
					</Form.Group>
					<Form.Group controlId="seqNumber">
						<Form.Label>Priority</Form.Label>
						<Form.Control
							type="number"
							value={handledTask.seqNumber}
							onChange={this.handleNumTaskChange}
						/>
					</Form.Group>
					<Form.Group controlId="estimatedTime">
						<Form.Label>Estimated time</Form.Label>
						<Form.Control
							type="time"
							value={TaskDialogue.secToTime(handledTask.estimatedTime)}
							onChange={this.handleTimeChange}
						/>
					</Form.Group>
					<Form.Group controlId="relatedTaskId">
						<Form.Label>Depends on:</Form.Label>
						<Form.Control
							as="select"
							value={handledTask.relatedTaskId}
							onChange={this.handleNumTaskChange}
						>
							{
								tasks.map((task, i) =>
									<option key={i} value={task.taskId}>{task.name}</option>)
							}
						</Form.Control>
					</Form.Group>
					<Form.Group controlId="done">
						<Form.Check
							label="Finished"
							checked={handledTask.done}
							onChange={e => this.handleTaskChange(e.target.id, e.target.checked)}
						/>
					</Form.Group>
					<Button variant="primary" type="submit">
						{this.props.task ? 'Edit' : 'Save'}
					</Button>
				</Form>
			);
		}

		return (
			<Modal show onHide={this.props.onHide} aria-labelledby="edit-dialogue-title" centered>
				<Modal.Header closeButton>
					<Modal.Title id="edit-dialogue-title">{this.props.task ? 'Edit task' : 'Add task'}</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					{body}
				</Modal.Body>
			</Modal>
		);
	}
}

export default TaskDialogue;
