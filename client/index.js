// Assets
import './styles/style.scss';

// Application
import ReactDOM from 'react-dom';
import React from 'react';
import AppRouter from './router';

import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';

// register icons for the applications
library.add(fas);

// Root path, in case the application is installed in a subdirectory
const basename = document.getElementsByTagName('base')[0].getAttribute('href');

ReactDOM.render(
	<AppRouter basename={basename} apiBase={basename + '/api/v1'} />,
	document.getElementById('root')
);
