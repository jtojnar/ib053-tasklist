import PropTypes from 'prop-types';
import React from 'react';
import { Container, Row, Col, Form, Button } from 'react-bootstrap';


class SignInPage extends React.Component {
	static propTypes = {
		basename: PropTypes.string.isRequired,
		apiBase: PropTypes.string.isRequired,
		handleSignIn: PropTypes.func,
	}

	constructor(props) {
		super(props);
		this.state = {
			error: null,
			form: {
				login: '',
				password: '',
			},
		};

		this.signIn = this.signIn.bind(this);
		this.handleFieldChange = this.handleFieldChange.bind(this);
		this.handleStringFieldChange = this.handleStringFieldChange.bind(this);
	}

	handleFieldChange(key, value) {
		this.setState( prevState => ({ form :
			{
				...prevState.form,
				[key]: value
			}
		}));
	}

	handleStringFieldChange(e) {
		const key = e.target.id;
		const value = e.target.value;
		this.handleFieldChange(key, value);
	}

	signIn(event) {
		event.preventDefault();
		const { apiBase } = this.props;
		const { form } = this.state;

		Promise.all([
			fetch(new Request(
				`${apiBase}/login`,
				{
					method: 'POST',
					body: JSON.stringify({
						'login': form.login,
						'password': form.password,
					})
				})
			).then((resp) => resp.json()),
		]).then(
			([response]) => {
				if (response.status === 'error') {
					this.setState({
						error: {
							message: response.message,
							code: response.code,
						},
					});
				} else {
					this.props.handleSignIn(response);
				}
			},
			// Note: it's important to handle errors here
			// instead of a catch() block so that we don't swallow
			// exceptions from actual bugs in components.
			(error) => {
				this.setState({
					error,
				});
			}
		);
	}

	render() {
		const { error } = this.state;

		return (
			<Container>
				<Row>
					<h2>Tasklist <small className="text-muted">sign in</small></h2>
				</Row>
				<Row>
					<Col>
						<Form onSubmit={this.signIn}>
							{error && <div className="alert alert-danger" role="alert">
								{error.message}
							</div>}
							<Form.Group controlId="login">
								<Form.Label>User name</Form.Label>
								<Form.Control type="username" placeholder="ursula" value={this.state.login} onChange={this.handleStringFieldChange} />
							</Form.Group>
							<Form.Group controlId="password">
								<Form.Label>Password</Form.Label>
								<Form.Control type="password" placeholder="password" value={this.state.password} onChange={this.handleStringFieldChange} />
							</Form.Group>
							<Button variant="primary" type="submit">Sign in</Button>
						</Form>
					</Col>
				</Row>
			</Container>
		);
	}
}

export default SignInPage;
