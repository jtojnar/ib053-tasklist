import PropTypes from 'prop-types';
import React from 'react';
import { Container, Row, Col, Table, Button } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Link } from 'react-router-dom';

import TaskDialogue from '../components/TaskDialogue.js';

const ModalState = Object.freeze({
	NONE: Symbol('none'),
	ADD: Symbol('add'),
	DELETE: Symbol('delete'),
	EDIT: Symbol('edit'),
});

/**
 * Mark tasks as done when true.
 */
function strikeWhen(body, prop) {
	if (prop) {
		return <del title="Task is done">{body}</del>;
	} else {
		return body;
	}
}

class TasksPage extends React.Component {
	static propTypes = {
		match: PropTypes.shape({
			params: PropTypes.shape({
				id: PropTypes.string,
			}),
		}),
		apiBase: PropTypes.string.isRequired,
		apiToken: PropTypes.string.isRequired,
	};

	constructor(props) {
		super(props);
		this.state = {
			error: null,
			showDone: false,
			isLoaded: false,
			tasks: [],
			showModal: ModalState.NONE,
			selectedTask: null,
		};

		this.deleteTask = this.deleteTask.bind(this);
		this.loadTasks = this.loadTasks.bind(this);
	}

	componentDidMount() {
		this.loadTasks();
	}

	loadTasks() {
		const { apiBase, apiToken, match } = this.props;
		fetch(`${apiBase}/user/${match.params.id}/tasks`, {
			headers: {
				Authorization: `Bearer ${apiToken}`,
			},
		})
			.then((tasks) => tasks.json())
			.then(
				(tasks) => {
					this.setState({
						isLoaded: true,
						tasks: tasks,
					});
				},
				// Note: it's important to handle errors here
				// instead of a catch() block so that we don't swallow
				// exceptions from actual bugs in components.
				(error) => {
					this.setState({
						isLoaded: true,
						error,
					});
				}
			);
	}

	deleteTask(taskId) {
		const { apiToken, apiBase } = this.props;
		Promise.all([
			fetch(`${apiBase}/task/${taskId}`, {
				method: 'DELETE',
				headers: {
					Authorization: `Bearer ${apiToken}`,
				},
			})
		]).then(
			() => {
				this.setState({
					isLoaded: true,
					tasks: this.state.tasks.filter((task) => task.taskId !== taskId)
				});
			},
			// Note: it's important to handle errors here
			// instead of a catch() block so that we don't swallow
			// exceptions from actual bugs in components.
			(error) => {
				this.setState({
					isLoaded: true,
					error,
				});
			}
		);
	}

	render() {
		const { error, isLoaded, tasks, showDone } = this.state;
		const { match } = this.props;
		let body;
		if (error) {
			body = <div>Error: {error.message}</div>;
		} else if (!isLoaded) {
			// TODO: connect with the spinner
			body = <div>Loading…</div>;
		} else {
			let rows = tasks.filter(({ done }) => showDone || !done).map(({ taskId, name, done, estimatedTime }) => {
				return (
					<tr key={taskId}>
						<td>{strikeWhen(name, done)}</td>
						<td>
							<FontAwesomeIcon icon="clock" />{TaskDialogue.secToTime(estimatedTime)}
						</td>
						<td>
							<Button
								variant="link"
								onClick={() =>
									this.setState({ selectedTask: taskId, showModal: ModalState.EDIT })}
							><FontAwesomeIcon icon="edit" /></Button>
							<Button
								variant="link"
								onClick={() =>
									confirm(`Do you really want to delete task ${name}?`) &&
									this.deleteTask(taskId)}
							><FontAwesomeIcon icon="trash" /></Button>
						</td>
					</tr>
				);
			});
			body = (
				<React.Fragment>
					<p>
						<Link to="/"><FontAwesomeIcon icon="chevron-left" /> Back to list of users</Link>

						<Button variant="link" onClick={() => this.setState({ selectedTask: null, showModal: ModalState.ADD })}><FontAwesomeIcon icon="plus" /> Add new task</Button>

						<label>
							<input type="checkbox" onChange={({target}) => this.setState({showDone: target.checked})} value={showDone} />
							Show done tasks
						</label>
					</p>
					{rows.length ?
						<Table striped bordered hover>
							<tbody>
								{rows}
							</tbody>
						</Table>
						:
						<p>No {showDone || 'unfinished'} tasks available for this user.</p>
					}
				</React.Fragment>
			);
		}

		let modalClose = () => this.setState({ showModal: ModalState.NONE, selectedTask: null });

		let userId = parseInt(match.params.id, 10);

		return (
			<Container>
				<Row>
					<h2>Tasklist</h2>
				</Row>
				<Row>
					<Col>
						{body}
						{this.state.showModal === ModalState.ADD &&
							<TaskDialogue
								onHide={modalClose}
								apiBase={this.props.apiBase}
								apiToken={this.props.apiToken}
								userId={userId}
								reloadTasks={this.loadTasks}
							/>
						}
						{this.state.showModal === ModalState.EDIT &&
							<TaskDialogue
								onHide={modalClose}
								apiBase={this.props.apiBase}
								apiToken={this.props.apiToken}
								userId={userId}
								task={tasks.find(({ taskId }) => taskId === this.state.selectedTask)}
								reloadTasks={this.loadTasks}
							/>
						}
					</Col>
				</Row>
			</Container>
		);
	}
}

export default TasksPage;
