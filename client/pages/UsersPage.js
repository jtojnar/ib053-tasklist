import PropTypes from 'prop-types';
import React from 'react';
import { Container, Row, Col, Table } from 'react-bootstrap';
import { Link } from 'react-router-dom';

class UsersPage extends React.Component {
	static propTypes = {
		apiBase: PropTypes.string.isRequired,
		apiToken: PropTypes.string.isRequired,
	}

	constructor(props) {
		super(props);
		this.state = {
			error: null,
			isLoaded: false,
			users: [],
		};
	}

	componentDidMount() {
		const { apiBase, apiToken } = this.props;
		fetch(`${apiBase}/user`, {
			headers: {
				Authorization: `Bearer ${apiToken}`,
			},
		})
			.then((users) => users.json())
			.then(
				(users) => {
					this.setState({
						isLoaded: true,
						users: users,
					});
				},
				// Note: it's important to handle errors here
				// instead of a catch() block so that we don't swallow
				// exceptions from actual bugs in components.
				(error) => {
					this.setState({
						isLoaded: true,
						error,
					});
				}
			);
	}

	render() {
		const { error, isLoaded, users } = this.state;
		let body;
		if (error) {
			body = <div>Error: {error.message}</div>;
		} else if (!isLoaded) {
			// TODO: connect with the spinner
			body = <div>Loading…</div>;
		} else {
			body =
				<Table striped bordered hover>
					<tbody>
						{users.map(({ userId, fullName }) =>
							<tr key={userId}>
								<td><Link to={'/user/' + userId}>{fullName}</Link></td>
							</tr>
						)}
					</tbody>
				</Table>;
		}

		return (
			<Container>
				<Row>
					<h2>Tasklist <small className="text-muted">team members</small></h2>
				</Row>
				<Row>
					<Col>
						{body}
					</Col>
				</Row>
			</Container>
		);
	}
}

export default UsersPage;
