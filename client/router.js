import PropTypes from 'prop-types';
import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { Button } from 'react-bootstrap';

import SignInPage from './pages/SignInPage';
import TasksPage from './pages/TasksPage';
import UsersPage from './pages/UsersPage';



class NoMatch extends React.Component {
	static propTypes = {
		location: PropTypes.shape({
			pathname: PropTypes.string.isRequired,
		}),
	}

	render() {
		const { location } = this.props;
		return (
			<div>
				<h3>
					No match for <code>{location.pathname}</code>
				</h3>
			</div>
		);
	}
}

class AppRouter extends React.Component {
	static propTypes = {
		basename: PropTypes.string.isRequired,
		apiBase: PropTypes.string.isRequired,
	}

	constructor(props) {
		super(props);
		this.state = {
			user: JSON.parse(window.localStorage.getItem('user')),
		};

		this.handleSignIn = this.handleSignIn.bind(this);
		this.handleSignOut = this.handleSignOut.bind(this);
	}

	handleSignIn(response) {
		window.localStorage.setItem('user', JSON.stringify(response));
		this.setState({
			user: response,
		});
	}

	handleSignOut() {
		window.localStorage.removeItem('user');
		this.setState({
			user: null,
		});
	}

	render() {
		if (this.state.user === null) {
			return (
				<SignInPage basename={this.props.basename} apiBase={this.props.apiBase} handleSignIn={this.handleSignIn} />
			);
		} else {
			return (
				<React.Fragment>
					<Button className="sign-out-link" variant="link" onClick={this.handleSignOut}>Sign out</Button>
					<BrowserRouter basename={this.props.basename}>
						<Switch>
							<Route path="/" exact component={props => <UsersPage {...props} apiBase={this.props.apiBase} apiToken={this.state.user.token} />} />
							<Route path="/user/:id" component={props => <TasksPage {...props} apiBase={this.props.apiBase} apiToken={this.state.user.token} />} />
							<Route component={NoMatch} />
						</Switch>
					</BrowserRouter>
				</React.Fragment>
			);
		}
	}
}

export default AppRouter;
