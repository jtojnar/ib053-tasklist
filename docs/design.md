# Design
Application is split into two parts: a RESTful API written in PHP using [Nette](https://nette.org/) and a browser client using [React](https://reactjs.org/).
The source code of backend is mainly in the `app/` directory, whereas the frontend occupies `client/` directory. The `www/` directory serves as a web server root and it directs the user appropriately.

## Backend
Nette uses `RouterFactory` to determine how to handle HTTP request. It passes every request to `/api` path to relevant presenter and all other requests to `ClientPresenter` so we can handle the routing in the browser.

### Database design
![](ERDiagram.png)

### API Design
https://tasklist8.docs.apiary.io/

## Client
The client uses features from EcmaScript 6 standard which browsers do not support at the moment. We use WebPack to transpile it to something they like better. The compiled files will be placed into `www/dist/` from where the `ClientPresenter` will serve them to the user.
