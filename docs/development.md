## Useful commands
### Frontend
#### Building assets
* `npm run dev` – Run a program that will watch the client assets for changes and rebuild the bundle when necessary
* `npm run build` – Build the assets bundle for production

#### Linting
* `npm run eslint` – Check the JavaScript code for style and other issues
* `npm run eslint -- --fix` – Check the JavaScript code for style and other issues and attempt to fix them

### Backend
#### Create new user
* `php bin/create-user.php "Ursula Franklin" "admin" "password"`

#### Migrations
TODO

#### Linting
* `composer lint` – Check for syntax errors
* `composer php-cs-fixer` – Check coding style ([PHPStorm integration](https://www.jetbrains.com/help/phpstorm/using-php-cs-fixer.html))
* `composer phpstan` – Perform static analysis

#### Extras
* `git update-index --skip-worktree app/config/local.neon` – Ignore changes in local configuration ([source](https://medium.com/@igloude/git-skip-worktree-and-how-i-used-to-hate-config-files-e84a44a8c859))
