# Interesting links
## Backend
* https://forum.nette.org/cs/31860-vse-o-vydani-nette-3-0-prubezne-aktualizovano Cutting edge version of Nette

## Client
* https://github.com/gothinkster/react-redux-realworld-example-app Medium complexity example React app – ignore the Redux parts
* https://fontawesome.com/how-to-use/on-the-web/using-with/react icons
* https://react-bootstrap.github.io/getting-started/introduction/ Twitter Bootstrap library for React
* https://reacttraining.com/react-router/web/guides/quick-start React router
* https://reactjs.org/docs/forms.html Forms in react
