# Tasklist
This is a simple task list application for IB053 course.

## Installation
Install the dependencies using [Composer](https://getcomposer.org/) and [npm](https://www.npmjs.com/). You will also need [Webpack](https://webpack.js.org/) to build the client side application.

	composer install
	npm install
    npm run build

Make directories `temp/` and `log/` writable.

You can also use `npm run dev` to watch changes in the client assets and have the bundle files rebuilt when it happens.
