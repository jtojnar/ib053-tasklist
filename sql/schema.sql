﻿DROP TABLE IF EXISTS user;
CREATE TABLE user (
  id INT(11) AUTO_INCREMENT PRIMARY KEY,
  login VARCHAR(255) COLLATE utf8_unicode_ci NOT NULL UNIQUE,
  password VARCHAR(255) COLLATE utf8_unicode_ci NOT NULL,
  full_name VARCHAR(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS task;
CREATE TABLE task (
  id INT(11) AUTO_INCREMENT PRIMARY KEY,
  task_name VARCHAR(255) COLLATE utf8_unicode_ci NOT NULL,
  assigned_user_id INT(11) NOT NULL,
  previous_required_task_id INT(11) DEFAULT NULL,
  seq_number INT(11) NOT NULL,
  estimated_time INT(11) NOT NULL,
  done BOOLEAN NOT NULL,
  FOREIGN KEY (assigned_user_id) REFERENCES user(id),
  FOREIGN KEY (previous_required_task_id) REFERENCES task(id) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS token;
CREATE TABLE token (
  token_hash VARCHAR(255) COLLATE utf8_unicode_ci PRIMARY KEY,
  valid_until TIMESTAMP NOT NULL,
  user_id INT(11) NOT NULL,
  FOREIGN KEY (user_id) REFERENCES user(id) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
